import {
  Body,
  Controller,
  Delete,
  Get,
  HttpStatus,
  Inject,
  Param,
  Post,
  Put,
  Res,
} from '@nestjs/common';
import { ClientKafka } from '@nestjs/microservices';
import { AppService } from './app.service';
import { Issues } from './entities/issues.entity';
import { kafkaLog } from './middleware/logger.middleware';

@Controller('issues')
export class AppController {
  constructor(
    private readonly appService: AppService,
    @Inject('CUD-SERVICE') private readonly client: ClientKafka,
  ) {}

  async onModuleInit() {
    this.client.subscribeToResponseOf('delete.issue');
    await this.client.connect();
  }

  async onModuleDestroy() {
    await this.client.close();
  }

  @Get()
  sendHello(@Res() response) {
    this.client.emit('riset.kafka', 'HEEEEEELLLLLL NOOOO!');
    return response.status(HttpStatus.OK).json({ status: 'success' });
  }

  @Post('/create')
  async createIssue(@Res() response, @Body() issueData: Issues) {
    const issue = await this.appService.createIssue(issueData);
    this.client.emit('create.issue', issueData);
    return response.status(HttpStatus.CREATED).json(issue);
  }

  @Put(':id')
  async updateIssue(
    @Res() response,
    @Param('id') id,
    @Body() issueData: Issues,
  ) {
    await this.appService.updateIssue(id, issueData);
    issueData.id = id;
    this.client.emit('update.issue', issueData);
    return response.status(HttpStatus.OK).json({ status: 'Success' });
  }

  @Delete(':id')
  async deleteIssue(@Res() response, @Param('id') id) {
    await this.appService.deleteIssue(id);

    this.client.send('delete.issue', id).forEach((value) => {
      kafkaLog.log('Response from List-Service: ' + value);
    });

    return response.status(HttpStatus.OK).json({ status: 'Success' });
  }
}
