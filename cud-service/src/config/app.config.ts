import DatabaseConfig from './database.config';
import KafkaConfig from './kafka.config';

export default () => ({
  environment: process.env.NODE_ENVIRONMENT
    ? process.env.NODE_ENVIRONMENT
    : 'development',
  port: 4000,
  database: {
    ...DatabaseConfig(),
  },
  broker: process.env.BROKER_HOST,
  kafka: {
    ...KafkaConfig(),
  },
});
