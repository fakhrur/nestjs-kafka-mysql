import {MigrationInterface, QueryRunner} from "typeorm";

export class CreateIssuesTable1637670027276 implements MigrationInterface {
    name = 'CreateIssuesTable1637670027276'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE \`issues\` (\`id\` int NOT NULL AUTO_INCREMENT, \`shortname\` varchar(80) NOT NULL, \`human_shortname\` varchar(80) NOT NULL, \`severity_int\` int NOT NULL, \`severity_ext\` int NOT NULL, \`cve_info\` varchar(255) NULL, \`intrusiveness_value\` int NOT NULL, \`confidence_level\` int NOT NULL, \`fix_difficulty\` int NOT NULL, \`fp_check_difficulty\` int NOT NULL, \`title\` varchar(255) NOT NULL, \`issue_category\` varchar(255) NOT NULL, \`issue_subcategory\` varchar(255) NOT NULL, \`description\` text NOT NULL, \`risk\` text NOT NULL, \`mitigation\` text NOT NULL, \`additional_mitigation_step\` text NULL, \`mitigation_resource\` text NULL, \`is_simple\` tinyint NOT NULL DEFAULT 1, \`is_extern_only\` tinyint NOT NULL DEFAULT 0, \`is_disabled\` tinyint NOT NULL DEFAULT 0, \`overwrites\` varchar(255) NULL, \`checks\` varchar(255) NULL, \`common_ports\` varchar(255) NULL, \`search_query\` varchar(255) NULL, \`created_on\` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP, \`updated_on\` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP, PRIMARY KEY (\`id\`)) ENGINE=InnoDB`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`DROP TABLE \`issues\``);
    }

}
