import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { ClientsModule } from '@nestjs/microservices';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ConnectionOptions } from 'typeorm';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import AppConfig from './config/app.config';
import { Issues } from './entities/issues.entity';

@Module({
  imports: [
    ConfigModule.forRoot({
      load: [AppConfig],
      isGlobal: true,
    }),
    ClientsModule.registerAsync([
      {
        name: 'CUD-SERVICE',
        imports: [ConfigModule],
        useFactory: async (configService: ConfigService) => {
          return configService.get('kafka');
        },
        inject: [ConfigService],
      },
    ]),
    TypeOrmModule.forRootAsync({
      imports: [ConfigModule],
      useFactory: (configService: ConfigService) => {
        return configService.get<ConnectionOptions>('database');
      },
      inject: [ConfigService],
    }),
    TypeOrmModule.forFeature([Issues]),
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
