import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ConnectionOptions } from 'typeorm';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import AppConfig from './config/app.config';
import { Issues } from './entities/issues.entity';

@Module({
  imports: [
    ConfigModule.forRoot({
      load: [AppConfig],
      isGlobal: false,
    }),
    TypeOrmModule.forRootAsync({
      name: 'list-db-connection',
      imports: [ConfigModule],
      useFactory: (configService: ConfigService) => {
        return configService.get<ConnectionOptions>('database');
      },
      inject: [ConfigService],
    }),
    TypeOrmModule.forFeature([Issues], 'list-db-connection'),
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
