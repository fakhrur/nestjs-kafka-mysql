import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { DeleteResult, Repository, UpdateResult } from 'typeorm';
import { Issues } from './entities/issues.entity';

@Injectable()
export class AppService {
  constructor(
    @InjectRepository(Issues) private issuesRepository: Repository<Issues>,
  ) {}

  getHello(): string {
    return 'Hello World!';
  }

  createIssue(issue: Issues): Promise<Issues> {
    const result = this.issuesRepository.save(issue);
    return result;
  }

  updateIssue(id: number, issue: Issues): Promise<UpdateResult> {
    const result = this.issuesRepository.update(id, issue);
    return result;
  }

  deleteIssue(id: number): Promise<DeleteResult> {
    const result = this.issuesRepository.delete(id);
    return result;
  }
}
