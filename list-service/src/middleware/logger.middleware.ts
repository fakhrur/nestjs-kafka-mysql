import { Logger } from '@nestjs/common';
import { Request, Response, NextFunction } from 'express';

export const log = new Logger('List Service');

export const kafkaLog = new Logger('Kafka - List Service');

export function logger(req: Request, res: Response, next: NextFunction) {
  log.log(`Request to path: ${req.path} Method: ${req.method}`);
  next();
}
