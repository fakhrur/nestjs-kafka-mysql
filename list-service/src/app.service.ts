import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Issues } from './entities/issues.entity';
import { kafkaLog } from './middleware/logger.middleware';

@Injectable()
export class AppService {
  constructor(
    @InjectRepository(Issues, 'list-db-connection')
    private issuesRepository: Repository<Issues>,
  ) {}

  getHello(): string {
    return 'Hello World!';
  }

  createIssue(issue: Issues) {
    kafkaLog.log('Create New Issue with title: ' + issue.shortname);
    this.issuesRepository.save(issue);
  }

  updateIssue(issue: Issues) {
    kafkaLog.log('Update Issue data with ID: ', issue.id);
    this.issuesRepository.update(issue.id, issue);
  }

  deleteIssue(id: number) {
    kafkaLog.log('Delete Issue data with ID ' + id);
    this.issuesRepository.delete(id);
  }

  getAllIssues(): Promise<Issues[]> {
    return this.issuesRepository.find();
  }
}
