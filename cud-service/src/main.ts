import { ConfigService } from '@nestjs/config';
import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { logger, log } from './middleware/logger.middleware';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.use(logger);
  const configService = app.get<ConfigService>(ConfigService);
  await app.listen(configService.get<number>('port'), () => {
    log.log('API Listen on port ' + configService.get<number>('port'));
  });
}
bootstrap();
