import { Logger } from '@nestjs/common';
import { Request, Response, NextFunction } from 'express';

export const log = new Logger('CUD Service');

export const kafkaLog = new Logger('Kafka - CUD Service');

export function logger(req: Request, res: Response, next: NextFunction) {
  log.log(`Request to path: ${req.path} Method: ${req.method}`);
  next();
}
