import { Controller, Get, HttpStatus, Res } from '@nestjs/common';
import {
  Ctx,
  EventPattern,
  KafkaContext,
  MessagePattern,
  Payload,
} from '@nestjs/microservices';
import { AppService } from './app.service';
import { kafkaLog } from './middleware/logger.middleware';

@Controller('issues')
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get('/')
  sayHello(@Res() response) {
    return response.status(HttpStatus.OK).end();
  }

  @Get('/list')
  async getList(@Res() response) {
    const result = await this.appService.getAllIssues();
    return response.status(HttpStatus.OK).json(result);
  }

  @EventPattern('create.issue')
  createIssue(@Payload() message) {
    this.appService.createIssue(message.value);
  }

  @EventPattern('update.issue')
  updateIssue(@Payload() message) {
    this.appService.updateIssue(message.value);
  }

  @EventPattern('riset.kafka')
  replyDelete(@Payload() message, @Ctx() context: KafkaContext) {
    const partition = context.getPartition();
    kafkaLog.log(
      'GOT Message: ' + message.value + ' from partition: ' + partition,
    );
  }

  @MessagePattern('delete.issue')
  deleteIssue(@Payload() message): string {
    this.appService.deleteIssue(message.value);
    return 'DELETE SUCCESS';
  }
}
