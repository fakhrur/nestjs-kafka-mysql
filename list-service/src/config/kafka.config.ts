import { Transport } from '@nestjs/microservices';
import { v4 as uuid } from 'uuid';

const KafkaConfig = () => ({
  transport: Transport.KAFKA,
  options: {
    client: {
      brokers: [process.env.BROKER_HOST],
    },
    consumer: {
      groupId: `issue.${uuid()}`,
    },
  },
});

export default KafkaConfig;
