import { Entity, PrimaryGeneratedColumn, Column } from 'typeorm';

@Entity('issues') // table name
export class Issues {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ nullable: false, length: 80 })
  shortname: string;

  @Column({ nullable: false, length: 80 })
  human_shortname: string;

  @Column()
  severity_int: number;

  @Column()
  severity_ext: number;

  @Column({ nullable: true })
  cve_info: string;

  @Column()
  intrusiveness_value: number;

  @Column()
  confidence_level: number;

  @Column()
  fix_difficulty: number;

  @Column()
  fp_check_difficulty: number;

  @Column({ nullable: false })
  title: string;

  @Column({ nullable: false })
  issue_category: string;

  @Column({ nullable: false })
  issue_subcategory: string;

  @Column({
    type: 'text',
    nullable: false,
  })
  description: string;

  @Column({
    type: 'text',
    nullable: false,
  })
  risk: string;

  @Column({
    type: 'text',
    nullable: false,
  })
  mitigation: string;

  @Column({ type: 'text', nullable: true })
  additional_mitigation_step: string;

  @Column({ type: 'text', nullable: true })
  mitigation_resource: string;

  @Column({ default: true })
  is_simple: boolean;

  @Column({ default: false })
  is_extern_only: boolean;

  @Column({ default: false })
  is_disabled: boolean;

  @Column({ nullable: true })
  overwrites: string;

  @Column({ nullable: true })
  checks: string;

  @Column({ nullable: true })
  common_ports: string;

  @Column({ nullable: true })
  search_query: string;

  @Column({
    type: 'datetime',
    default: () => 'CURRENT_TIMESTAMP',
  })
  created_on: Date;

  @Column({
    type: 'datetime',
    default: () => 'CURRENT_TIMESTAMP',
  })
  updated_on: Date;
}
